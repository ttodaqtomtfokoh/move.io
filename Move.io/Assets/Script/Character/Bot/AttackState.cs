﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IState
{
    public void OnEnter(BotControl bot)
    {
        bot.PreAttack();
    }

    public void OnExecute(BotControl bot)
    {

    }

    public void OnExit(BotControl bot)
    {
        throw new System.NotImplementedException();
    }

}
