﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class BotControl : CharacterControl
{
    [Header("Bot")]
    public GameObject targeted;
    private NavMeshAgent nav;

    internal void PreAttack()
    {
        throw new NotImplementedException();
    }

    protected void Awake()
    {
        base.Awake();
        nav = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        //nav.SetDestination(new Vector3(5, 0, 15));
        //OnChangeState(new IdleState());
    }

    public override void Die()
    {
        base.Die();
        GameManager.instance.BotDie();
        gameObject.SetActive(false);
        BotPooler.Instance.BotQueue.Enqueue(gameObject);
    }

    protected void Update()
    {
        base.Update();
        UpdateState();
        if (nav.velocity.sqrMagnitude > 0) is_moving = true;
        else is_moving = false;
    }

    public void UpdateMesh()
    {
        UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
    }

    //State
    private IState currentState;

    private void UpdateState()
    {
        if (currentState != null)
        {
            currentState.OnExecute(this);
        }
    }

    public void OnChangeState(IState state)
    {
        if (currentState != null)
        {
            currentState.OnExit(this);
        }

        currentState = state;

        if (currentState != null)
        {
            currentState.OnEnter(this);
        }
    }
}
