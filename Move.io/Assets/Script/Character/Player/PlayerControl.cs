﻿using UnityEngine;

public class PlayerControl : CharacterControl
{
    // Start is called before the first frame update
    [Header("Player")]
    public FloatingJoystick variableJoystick;
    public CamControl camera;
    private float speed;
    private BotControl last_target = null;

    protected void Awake()
    {
        base.Awake();
        speed = Const.CHARACTER_SPEED;
    }
    public void FixedUpdate()
    {
        if (canmove)
        {
            Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
            rb.velocity = direction * speed;
            Rotation();
            if (rb.velocity.sqrMagnitude > 0) is_moving = true;
            else is_moving = false;
        }
    }

    public void Rotation()
    {
        if (variableJoystick.Horizontal != 0 || variableJoystick.Vertical != 0)
        {
            Vector3 moveDir = new Vector3(variableJoystick.Horizontal, 0, variableJoystick.Vertical);
            transform.rotation = Quaternion.LookRotation(moveDir).normalized;
        }
    }

    public override void Kill(IHit victim)
    {
        base.Kill(victim);
        camera.ZoomOut();
    }

    public override void Die()
    {
        base.Die();
        //code lose
        GameManager.instance.Lose();
        gameObject.SetActive(false);
    }

    protected void Update()
    {
        base.Update();
        
        if (nearest != null) 
        {
            if (nearest.GetComponent<BotControl>() != last_target)
            {
                if (last_target != null)
                {
                    last_target.targeted.SetActive(false);
                }
                last_target = nearest.GetComponent<BotControl>();
                last_target.targeted.SetActive(true);
            }
        }
        else
        {
            if (last_target != null)
            {
                last_target.targeted.SetActive(false);
                last_target = null;
            }
        }
        
    }
}
