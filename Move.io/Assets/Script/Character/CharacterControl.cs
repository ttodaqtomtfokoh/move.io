﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CharacterControl : MonoBehaviour, IHit
{
    [Header("Character")]
    [SerializeField] private Transform arm;
    [SerializeField] private GameObject WeaponOnHand;
    [SerializeField] private WeaponControl WeaponShoot;
    [SerializeField] private Animator anim;
    [SerializeField] private Transform scoreDisplay;
    [SerializeField] private TextMesh scoreText;

    [HideInInspector] public float range;
    [HideInInspector] public string Name;
    [HideInInspector] public int score;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public bool is_moving = false;
    [HideInInspector] public Transform nearest;
    [HideInInspector] public bool canmove = true;
    
    private Collider[] PhysicOVerlap;
    private List<IHit> TargetInRange;
    private float cooldown;
    private bool has_target = false;
    private bool Attack_ready = true;

    public Transform GetTransform()
    {
        return transform;
    }

    protected void Awake()
    {
        range = Const.CHARACTER_RANGE;
        cooldown = Const.CHARACTER_COOLDOWN;
        rb = GetComponent<Rigidbody>();
        TargetInRange = new List<IHit>();
        WeaponShoot.character = this;
    }

    private void DetectTarget()
    {
        //add target to list
        PhysicOVerlap = Physics.OverlapSphere(transform.position, range);

        for (int i = 0; i < PhysicOVerlap.Length; ++i)
        {
            CharacterControl Target = PhysicOVerlap[i].GetComponent<CharacterControl>();
            if (Target != null && Target != this)
            {
                TargetInRange.Add(PhysicOVerlap[i].GetComponent<CharacterControl>());
                TargetInRange = TargetInRange.Distinct().ToList();
            }
        }

        //remove outrange target
        for (int i = 0; i < TargetInRange.Count; ++i)
        {
            float distance = Vector3.Distance(transform.position, TargetInRange[i].GetTransform().position);
            if (distance > range)
            {
                TargetInRange.RemoveAt(i);
            }
        }

        //return nearest enemy
        int count = TargetInRange.Count;
        if (count == 0)
        {
            nearest = null;
            has_target = false;
        }
        else if (count == 1)
        {
            nearest = TargetInRange[0].GetTransform();
            has_target = true;
        }
        else
        {
            has_target = true;
            float min_distance = range;
            int min_enemy = 0;
            for (int i = 0; i < count; ++i)
            {
                float distance = Vector3.Distance(transform.position, TargetInRange[i].GetTransform().position);
                if (distance < min_distance)
                {
                    min_distance = distance;
                    min_enemy = i;
                }
            }
            nearest = TargetInRange[min_enemy].GetTransform();
        }

    }

    private void Attack(Transform target)
    {
        canmove = false;
        //Vector3 lookat = target.position;
        //lookat.y = 0;
        //transform.DOLookAt(lookat, 0.1f).OnComplete(() => {
        //    Vector3 move = target.position;
        //    move.y = arm.position.y;
        //    WeaponOnHand.transform.DOMove(move, 1).OnComplete(() =>
        //    {
        //        anim.SetBool("IsAttack", false);
        //        Attack_ready = true;
        //        WeaponOnHand.SetActive(true);
        //        WeaponOnHand.transform.position = arm.position;
        //        canmove = true;
        //    });
        //});
        //Attack_ready = false;

        //TODO: call movetotarget func from weapon control
        Vector3 lookat = target.position;
        lookat.y = 0;
        transform.DOLookAt(lookat, 0.1f).OnComplete(() =>
        {
            WeaponShoot.gameObject.SetActive(true);
            WeaponShoot.TargetDirection(target.position);
        });
    }

    public virtual void Die() { }

    public virtual void Kill(IHit victim)
    {
        Debug.Log(this.ToString() + " kill " + victim.ToString());
        canmove = true;
        TargetInRange.Remove(victim);
        score += 1;
        scoreText.text = score.ToString();
        transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        range *= 1.1f;
    }

    protected void Update()
    {
        DetectTarget();
        scoreDisplay.rotation = Quaternion.Euler(transform.position * -1);
        if (is_moving)
        {
            anim.SetBool("IsIdle", false);
        }
        else
        {
            anim.SetBool("IsIdle", true);
            if (has_target && Attack_ready)
            {
                anim.SetBool("IsAttack", true);
                Attack(nearest);
            }
        }
    }
}
