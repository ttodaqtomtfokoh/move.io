﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponControl : MonoBehaviour
{
    [HideInInspector] public CharacterControl character;
    public Vector3 startpos;
    public Vector3 directionToTarget;
    public float speed;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        IHit target = other.gameObject.GetComponent<IHit>();
        if (target != null)
        {
            gameObject.SetActive(false);
            character.Kill(target);
            target.Die();
        }
    }

    public void TargetDirection(Vector3 targetPos)
    {
        directionToTarget = (targetPos - startpos).normalized;
    }

    public void MoveToTarget()
    {

        Quaternion lookTarget = Quaternion.LookRotation(directionToTarget);

        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookTarget, Time.deltaTime * 30f).eulerAngles;

        transform.Translate(directionToTarget * Time.deltaTime * speed, Space.World);

        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    private void OutRange(float ownerRange)
    {
        //set active false
    }

    void Update()
    {
        MoveToTarget();
    }
}
