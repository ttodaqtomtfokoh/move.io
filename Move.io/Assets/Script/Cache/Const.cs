﻿public class Const
{
    public const float CHARACTER_SPEED = 2;
    public const float CHARACTER_RANGE = 3;
    public const float CHARACTER_COOLDOWN = 1;

    public const int PLAYER_ALIVE = 20;
    public const int BOT_ONMAP = 5;
}
