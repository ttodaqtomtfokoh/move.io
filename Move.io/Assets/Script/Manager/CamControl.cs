﻿using UnityEngine;
using DG.Tweening;

public class CamControl : MonoBehaviour
{
    public Transform target;
    [SerializeField]
    private Transform camTransform;

    public Vector3 Offset;
    [SerializeField]
    private Vector3 velocity = Vector3.zero;

    public float SmoothTime = 0.3f;

    void Start()
    {
        Offset = camTransform.position - target.position;
    }

    void LateUpdate()
    {
        Vector3 targetPosition = target.position + Offset;
        targetPosition = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z);
        camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);
    }

    public void ZoomOut()
    {
        GetComponent<Camera>().fieldOfView += 1;
        Offset += new Vector3(0,0.25f,0);
    }
}