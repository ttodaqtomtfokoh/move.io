﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    /*[HideInInspector]*/ public int alive;
    void Awake()
    {
       if (instance == null)
        {
            instance = this;
        }
       else
       {
            Destroy(gameObject);
       }
    }
    // Start is called before the first frame update
    void Start()
    {
        alive = Const.PLAYER_ALIVE;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BotDie()
    {
        alive -= 1;
        if (alive == 1)
        {
            Debug.Log("you win");
        }
        else if (alive > 1)
        {
            StartCoroutine(BotPooler.Instance.RespawnBot());
        }
    }

    public void Lose()
    {
        Debug.Log("you lose");
    }
}
