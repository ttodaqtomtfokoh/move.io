﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class pool
{
    public GameObject bot;
    public int size;
}
public class BotPooler : MonoBehaviour
{
    public static BotPooler Instance;
    public Queue<GameObject> BotQueue = new Queue<GameObject>();
    public pool botpool;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            for (int i = 0; i < botpool.size; i++)
            {
                GameObject new_bot = Instantiate(botpool.bot);
                new_bot.SetActive(false);
                BotQueue.Enqueue(new_bot);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public GameObject SpawnBot()
    {
        GameObject BotToSpawn = BotQueue.Dequeue();
        if (BotQueue.Count == 0)
        {
            BotQueue.Enqueue(BotToSpawn);
        }
        BotToSpawn.SetActive(true);
        BotToSpawn.transform.position = RandomPos();
        return BotToSpawn;
    }

    public IEnumerator RespawnBot()
    {
         yield return new WaitForSeconds(1);
         SpawnBot();
    }

    private Vector3 RandomPos()
    {
        return new Vector3(Random.Range(-15f, 15f), 0, Random.Range(-15f, 15f));
    }
}
