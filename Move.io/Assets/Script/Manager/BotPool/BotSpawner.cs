﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotSpawner : MonoBehaviour
{

    private void Start()
    {
        for (int i = 0; i < Const.BOT_ONMAP; i++)
        {
            BotPooler.Instance.SpawnBot();
        }
    }
}
