﻿using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public FloatingJoystick variableJoystick;
    public Rigidbody rb;

    public void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        rb.velocity = direction * speed;
        Rotation();
    }

    public void Rotation()
    {
        if (variableJoystick.Horizontal != 0 || variableJoystick.Vertical != 0)
        {
            Vector3 moveDir = new Vector3(variableJoystick.Horizontal, 0, variableJoystick.Vertical);
            transform.rotation = Quaternion.LookRotation(moveDir).normalized;
        }
    }
}